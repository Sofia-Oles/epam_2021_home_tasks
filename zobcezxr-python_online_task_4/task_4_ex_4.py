"""
Implement a function `split_by_index(string: str, indexes: List[int]) -> List[str]`
which splits the `string` by indexes specified in `indexes`.
Only positive index, larger than previous in list is considered valid.
Invalid indexes must be ignored.

Examples:
```python
# >>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

# >>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

# >>> split_by_index("no luck", [42])
["no luck"]
```
"""


def split_by_index(string, indexes):
    result_list, index = [], 0
    for i in indexes:
        next_index = i
        if isinstance(i, int) is False or i < 0 or next_index <= index or i >= len(string):
            continue
        result_list.append(string[index:next_index])
        index = next_index
    result_list.append(string[index:])
    return result_list

