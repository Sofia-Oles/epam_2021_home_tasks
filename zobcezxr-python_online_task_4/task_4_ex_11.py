"""
Write a function `fibonacci_loop(seq: list)`, which accepts a list of values and
prints out values in one line on these conditions:
 - floating point numbers should be ignored
 - string values should stop the iteration
 - loop control statements should be used

Example:
# >>> fibonacci_loop([0, 1, 1.1, 1, 2, 99.9, 3, 0.0, 5, 8, "stop", 13, 21, 34])
0 1 1 2 3 5 8
"""


# Author: Sofia Oleskevych

def fibonacci_loop(seq: list) -> str:
    """
    Function filters a list with different data types

    :param seq: list we need to process
    :return: list consists of 'int' elements
    """
    result = ""
    for i in seq:
        if isinstance(i, int):
            result += str(i)
        elif isinstance(i, float):
            pass
        elif isinstance(i, str):
            break
    result = " ".join(map(str, result))
    return result


# print(fibonacci_loop([0, 1, 1.1, 13, 2, 99.9, 3, 0.0, 5, 8, "STOP", 13, 21, 34]))
