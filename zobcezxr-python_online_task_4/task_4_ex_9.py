"""
For a positive integer n calculate the result value, which is equal to the sum
of the odd numbers of n.

Example,
n = 1234 result = 4
n = 246 result = 0

Write it as function.

Note:
Raise TypeError in case of wrong data type or negative integer;
Use of 'functools' module is prohibited, you just need simple for loop.
"""


# Author: Sofia Oleskevych


def sum_odd_numbers(n: int) -> int:
    """
    Function takes an integer and returns a sum of all odd numbers

    :param n: integer we need to parse
    :return: sum of all odd numbers
    """
    result = 0
    if isinstance(n, int) and n > 0:
        list_of_numbers = list(map(int, str(n)))
        for i in list_of_numbers:
            if i % 2 == 1:
                result += i
        return result
    else:
        raise TypeError


# print(sum_odd_numbers(1234))
# print(sum_odd_numbers(246))
# print(get_pairs([1]))
