"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""


# Author: Sofia Oleskevych


def split_alternative(str_to_split: str, delimiter=" ") -> list:
    """
    str.split() implementation
    :param str_to_split, delimiter: string we need to separate with delimiter
    :return: list of separated slices
    """
    result_list, index = [], 0
    if isinstance(str_to_split, str):
        for _ in range(len(str_to_split)):
            if str_to_split[_] == delimiter:
                result_list.append(str_to_split[index:_])
                index = _ + 1
        result_list.append(str_to_split[index:len(str_to_split)])
    else:
        raise ValueError
    return result_list
