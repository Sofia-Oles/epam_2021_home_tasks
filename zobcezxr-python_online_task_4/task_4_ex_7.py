"""
Task04_1_7
Implement a function foo(List[int]) -> List[int] which, given a list of integers, returns a new  or modified list
in which every element at index i of the new list is the product of all the numbers in the original array except the one at i.
Example:
`python

foo([1, 2, 3, 4, 5])
[120, 60, 40, 30, 24]

foo([3, 2, 1])
[2, 3, 6]`
"""

# Author: Sofia Oleskevych

from typing import List


def multiply_list(pure_list: List[int]) -> int:
    result = 1
    for x in pure_list:
        result *= x
    return result


def product_array(num_list: List[int]) -> List[int]:
    test = num_list[:]
    res = []
    for i in range(len(num_list)):
        test.pop(i)
        res.insert(i, multiply_list(test))
        test.insert(i, num_list[i])
    return res
