"""
Task04_6

Implement a function get_longest_word(s: str) -> str which returns the longest word in the given string.
The word can contain any symbols except whitespaces (`,\n,\tand so on).
If there are multiple longest words in the string with a same length return the word that occurs first.

Example: get_longest_word('Python is simple and effective!')
         #output: 'effective!'
         get_longest_word('Any pythonista like namespaces a lot.')
         #output: 'pythonista'

Note:
Raise ValueError in case of wrong data type
Usage of 're' library is prohibited
"""

# Author: Sofia Oleskevych


def get_longest_word(str_to_parse: str) -> str:
    """
    Counting max word length
    :param str_to_parse: string we need to process
    :return: word with max length
    """
    res = ""
    if not isinstance(str_to_parse, str):
        raise ValueError
    splited = str_to_parse.split()
    for word in splited:
        if len(word) > len(res):
            res = word
    return res
