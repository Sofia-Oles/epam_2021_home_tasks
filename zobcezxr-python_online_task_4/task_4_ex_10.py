"""
Create a function sum_binary_1 that for a positive integer n
calculates the result value, which is equal to the sum of the
“1” in the binary representation of n otherwise, returns None.

Example,
n = 14 = 1110 result = 3
n = 128 = 10000000 result = 1
"""


# Author: Sofia Oleskevych

def sum_binary_1(n: int):
    """
    Function converts an integer to binary format

    :param n: integer we need to convert
    :return: sum of "1"
    """
    bin_str = ""
    if isinstance(n, int) and n > 0:
        while n > 0:
            reminder_of_division = n % 2
            n = n // 2
            bin_str += str(reminder_of_division)
        return bin_str.count("1")
    return None


# print(sum_binary_1(14))
# print(sum_binary_1(128))
# print(sum_binary_1(6))

