"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""

# Author: Sofia Oleskevych


def swap_quotes(string: str) -> str:
    single_quotes, double_quotes = "'", '"'
    result_str = ""
    for element in string:
        if element == single_quotes:
            element = double_quotes
        elif element == double_quotes:
            element = single_quotes
        result_str += element
    return result_str
