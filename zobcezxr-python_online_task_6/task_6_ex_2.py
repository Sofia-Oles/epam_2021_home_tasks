# Author: Sofia Oleskevych
class Item:
    """
    A node in a unidirectional linked list.
   """

    def __init__(self, *data):
        self.data = list(data)


class CustomList(Item):
    """
    An unidirectional linked list.
    """

    def __init__(self, *data):
        super().__init__(*data)

    def append(self, value):
        self.data.append(value)
        return None

    def add_start(self, value):
        self.data.insert(0, value)
        return None

    def remove(self, value):
        if value in range(len(self)):
            self.data.remove(value)
        else:
            raise Exception

    def __getitem__(self, index):
        if index in range(len(self)):
            return self.data.__getitem__(index)
        else:
            raise Exception('Index out of bound')

    def __setitem__(self, index, data):
        if index in range(len(self)):
            self.data[index] = data
            return data
        else:
            raise Exception('Index out of bound')

    def __delitem__(self, index):
        if index in range(len(self)):
            del self.data[index]
        else:
            raise Exception('Index out of bound')

    def find(self, value):
        if value in self:
            return self.data.index(value)
        else:
            raise Exception

    def clear(self):
        return self.data.clear()

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        return iter(self.data)

    def __str__(self):
        return str(self.data)



# a = CustomList(1, 5, 7, 4)
# a.append(900)
# a.add_start(-100)
# # print(a.__setitem__(1,500))
# print(a.find(990))
# print(a.__dict__.items())
# b = CustomList(2)
# a.nextnode = b
# print(a.nextnode.__dict__.items())
