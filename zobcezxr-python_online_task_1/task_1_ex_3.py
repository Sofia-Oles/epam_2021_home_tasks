""" Write a Python-script that determines whether the input string is the correct entry for the
'formula' according EBNF syntax (without using regular expressions).
Formula = Number | (Formula Sign Formula)
Digit = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
Number = Digit{Digit}
Sign = '+' | '-'
Input: string
Result: (True / False, The result value / None)

Example,
user_input = '1+2+4-2+5-1' result = (True, 9)
user_input = '123' result = (True, 123)
user_input = '-123' result = (False, None)
user_input = 'hello+12' result = (False, None)
user_input = '2++12--3' result = (False, None)
user_input = '' result = (False, None)

Example how to call the script from CLI:
python task_1_ex_3.py 1+5-2

Hint: use argparse module for parsing arguments from CLI
"""
import argparse


def calculate(operand_1, sign, operand_2) -> int:
    """Calculate expression"""
    if sign is None:
        return operand_2
    elif sign == '+':
        return operand_1 + operand_2
    elif sign == '-':
        return operand_1 - operand_2


def check_formula(user_input) -> tuple:
    """Check string from input"""

    number = None
    previous = 0
    sign = None

    for element in user_input:
        if element.isdigit():
            if number is None:
                number = 0
            number = number * 10 + int(element)  # 120 + 3
        elif element in ["+", "-"]:
            if number is None:
                return False, None
            previous = calculate(previous, sign, number)
            sign = element
            number = None
        else:
            return False, None

    if not user_input:
        return False, None

    return True, calculate(previous, sign, number)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('user_input', nargs='?')
    args = parser.parse_args()
    print(check_formula(args.user_input))


if __name__ == '__main__':
    main()

'''
Test data
"1+2+4-2+5-1"
"123"
"hello+12"
"2++12-3"
""
'''
