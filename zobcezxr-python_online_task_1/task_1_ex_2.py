"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""


# Author: Sofia Oleskevych

import argparse
import math
import operator


def calculate(func, *args):
    """Returns the calculation result of one or two operands."""

    call_func = getattr(math, func, None) or getattr(operator, func, None)
    if call_func is None:
        raise NotImplementedError()

    result_arg = [_ for _ in list(args) if _]
    return call_func(*result_arg)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("function", type=str)
    parser.add_argument("operand_1", type=float)
    parser.add_argument("operand_2", nargs='?', type=float)

    args = parser.parse_args()
    print(calculate(args.function, args.operand_1, args.operand_2))


if __name__ == '__main__':
    main()
