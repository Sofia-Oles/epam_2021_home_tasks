"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""

# Author: Sofia Oleskevych

import argparse


def calculate(operand_1, operator, operand_2):
    """Returns the calculation result of two operands."""

    if operator == "+":
        return operand_1 + operand_2
    elif operator == "-":
        return operand_1 - operand_2
    elif operator == "*":
        return operand_1 * operand_2
    elif operator == "/":
        if operand_2 != 0:
            return operand_1 / operand_2
        else:
            raise ZeroDivisionError
    else:
        raise NotImplementedError


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("operand_1", type=float)
    parser.add_argument("operator", type=str)
    parser.add_argument("operand_2", type=float)

    args = parser.parse_args()
    print(calculate(**vars(args)))


if __name__ == '__main__':
    main()
