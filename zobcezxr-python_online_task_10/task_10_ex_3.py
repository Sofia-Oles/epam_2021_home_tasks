import csv


def get_top_performers(file_path, number_of_top_students=5):
    with open(file_path) as f:
        data = csv.reader(f)
        temp = [el for ind, el in enumerate(data) if ind != 0]
        temp.sort(key=lambda x: float(x[2]), reverse=True)
        return [el[0] for ind, el in enumerate(temp) if ind < number_of_top_students]


def write_students_age_desc(file_path, output_file):
    with open(file_path) as f:
        data = csv.reader(f)
        temp = [el for ind, el in enumerate(data)]
    with open(output_file, 'w', newline='') as f:
        file_write = csv.writer(f)
        file_write.writerow(temp.pop(0))
        temp.sort(key=lambda x: float(x[1]), reverse=True)
        for i in temp:
            file_write.writerow(i)


#print(get_top_performers('data/input.csv', 6))
#write_students_age_desc('data/input.csv', 'data/output.csv')
