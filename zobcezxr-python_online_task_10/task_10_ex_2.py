import re
from collections import Counter


def most_common_words(text, top_words):
    result = []
    with open(text, 'r') as f:
        temp = f.read()
        temp = re.sub('[^a-zA-Z0-9]+', ' ', temp).split()
        count_words = Counter(temp)
        most_occur = count_words.most_common(top_words)
        for el in most_occur:
            result.append(el[0])
    return result
