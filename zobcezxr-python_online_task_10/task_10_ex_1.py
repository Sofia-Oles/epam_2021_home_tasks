def sort_names(input_file_path: str, output_file_path: str):
    with open(input_file_path, 'r', encoding='utf-8') as input_f:
        names = input_f.readlines()
        sorted_names = sorted(names)
    with open(output_file_path, 'w', encoding='utf-8') as output_f:
        output_f.writelines(name for name in sorted_names)
       
if __name__ == '__main__':
    from_ = 'C:/Users/soles/data/unsorted_names.txt'
    to_ = 'C:/Users/soles/data/sorted_names.txt'
    print('First 10 lines of the original file: ')
    with open(from_) as f:
        for i in range(10):
            print(f.readline().rstrip())
    print(f'\nWRITING {to_} FILE...')
    sort_names(from_, to_)
    print('\nFirst 10 lines of new file: ')
    with open(to_) as f:
        for i in range(10):
            print(f.readline().rstrip())