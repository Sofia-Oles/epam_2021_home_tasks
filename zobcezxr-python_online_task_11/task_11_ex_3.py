initial_arguments = None


def call_once(original_function):
    def wrapper_function(*args):
        global initial_arguments
        if initial_arguments is None:
            initial_arguments = [*args]
        return original_function(*initial_arguments)

    return wrapper_function


@call_once
def sum_of_numbers(a, b):
    return a + b


if __name__ == '__main__':
    print(sum_of_numbers(1, 42))
    print(sum_of_numbers(99, 100))
    print(sum_of_numbers(13, 14))