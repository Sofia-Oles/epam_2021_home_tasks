"""
Write 2 functions:
1. Function 'is_sorted', determining whether a given list of integer values of arbitrary length
is sorted in a given order (the order is set up by enum value SortOrder).
List and sort order are passed by parameters. Function does not change the array, it returns
boolean value.

2. Function 'transform', replacing the value of each element of an integer list with the sum
of this element value and its index, only if the given list is sorted in the given order
(the order is set up by enum value SortOrder). List and sort order are passed by parameters.
To check, if the array is sorted, the function 'is_sorted' is called.

Example for 'transform' function,
For [5, 17, 24, 88, 33, 2] and “ascending” sort order values in the array do not change;
For [15, 10, 3] and “ascending” sort order values in the array do not change;
For [15, 10, 3] and “descending” sort order the values in the array are changing to [15, 11, 5]

Note:
Raise TypeError in case of wrong function arguments data type;
"""


# Author: Sofia Oleskevych


from enum import Enum


class SortOrder(Enum):
    ascending = False
    descending = True


def is_sorted(lst: list, order: SortOrder) -> bool:
    if isinstance(lst, list) and all(isinstance(el, int) for el in lst) and isinstance(order, SortOrder):
        return sorted(lst, reverse=order.value) == lst
    raise TypeError


def transform(lst: list, order: SortOrder) -> list:
    new_list = []
    if not is_sorted(lst, order):
        return lst
    for element, index in enumerate(lst):
        element += index
        new_list.append(element)
    return new_list


# print(transform([15, 12, 11], SortOrder.descending))
