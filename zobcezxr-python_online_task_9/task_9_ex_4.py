"""
Implement a bunch of functions which receive a changeable number of strings and return next
parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
  Note: raise ValueError if there are less than two strings
4) characters of alphabet, that were not used in any string
  Note: use `string.ascii_lowercase` for list of alphabet letters

Note: raise TypeError in case of wrong data type

Examples,
```python
test_strings = ["hello", "world", "python", ]
print(chars_in_all(*test_strings))
Output: {'o'}
print(chars_in_one(*test_strings))
Output: {'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}
print(chars_in_two(*test_strings))
Output: {'h', 'l', 'o'}
print(not_used_chars(*test_strings))
Output: {'q', 'k', 'g', 'f', 'j', 'u', 'a', 'c', 'x', 'm', 'v', 's', 'b', 'z', 'i'}
"""
from itertools import chain
import string


def creation_temp(*strings):
    return ''.join(chain([c for str_ in strings for c in set(str_)]))


def chars_in_all(*strings):
    temp = creation_temp(*strings)
    return set(c for c in strings[0] if temp.count(c) == len(strings))


def chars_in_one(*strings):
    return set(''.join(strings))


def chars_in_two(*strings):
    temp = creation_temp(*strings)
    new = [c for c in strings[0] if temp.count(c) >= 2]
    if len(new):
        return set(new)
    else:
        raise ValueError


def not_used_chars(*strings):
    temp = creation_temp(*strings)
    return set([c for c in string.ascii_lowercase if c not in temp])
