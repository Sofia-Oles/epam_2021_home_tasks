"""
Implement a function get_longest_word(s: str) -> str which returns the
longest word in the given string. The word can contain any symbols
except whitespaces (`,\n,\t and so on). If there are multiple longest
words in the string with a same length return the word that occurs first.

Example:
get_longest_word('Python is simple and effective!')
#output: 'effective'
get_longest_word('Any pythonista like namespaces a lot.')
#output: 'pythonista'

Note:
Raise ValueError in case of wrong data type
Usage of 're' library is required.
"""
import re


def get_longest_word(t_string):
    if not isinstance(t_string, str):
        raise ValueError
    t_string = re.sub('[^a-zA-Z0-9]+', ' ', t_string)
    t_string = re.split(' ', t_string)
    print(t_string)
    count = [len(length) for length in t_string if length]
    index = count.index(max(count))
    return t_string[index]
