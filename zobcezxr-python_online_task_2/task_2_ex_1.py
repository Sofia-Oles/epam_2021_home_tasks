"""
Task 2_3

You are given n bars of gold with weights: w1, w2, ..., wn and bag with capacity W.
There is only one instance of each bar and for each bar you can either take it or not
(hence you cannot take a fraction of a bar). Write a function that returns the maximum weight of gold that fits
into a knapsack's capacity.

The first parameter contains 'capacity' - integer describing the capacity of a knapsack
The next parameter contains 'weights' - list of weights of each gold bar
The last parameter contains 'bars_number' - integer describing the number of gold bars
Output : Maximum weight of gold that fits into a knapsack with capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python task_2_ex_1.py -W 56 -w 3 4 5 6 -n 4
"""

# Author: Sofia Oleskevych
# DP approach

import argparse


def bounded_knapsack(w,W,n):
    F = [1] + [0] * W
    F_new = F[:]
    for j in range(n):
        for i in range(W, w[j] - 1, -1):
            if F[i - w[j]] == 1:
                F_new[i] = 1
        F = F_new
    i = W
    while F[i] == 0:
        i -= 1
    return i


def main():
    w = [19, 5, 5, 5, 5]
    W = 20
    n = len(w)

    print(bounded_knapsack(w,W,n))


if __name__ == '__main__':
    main()
